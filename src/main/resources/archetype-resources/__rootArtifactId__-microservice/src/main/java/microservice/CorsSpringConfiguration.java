package nl.timeseries.training.microservice;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import static org.springframework.security.config.http.SessionCreationPolicy.STATELESS;

@Configuration
public class CorsSpringConfiguration {

	@Bean // This bean is used to disable authentication on HTTP OPTIONS calls
	public WebSecurityConfigurerAdapter alwaysAllowHttpOptions() {
		return new WebSecurityConfigurerAdapter() {
			@Override
			protected void configure(HttpSecurity http) throws Exception {
				http
						.sessionManagement().sessionCreationPolicy(STATELESS) // We only provide stateless services
						.and()
						.csrf().disable() // Disabled so we can use the Swagger UI to test our endpoints
						// Swagger UI in Chrome does an unauthenticated OPTIONS call before an authenticated GET
						// This stops Spring from rejecting that call
						.authorizeRequests().antMatchers(HttpMethod.OPTIONS, "/admin/**").permitAll()
						.and()
						.httpBasic();
			}
		};
	}

	@Bean // This allows CORS to our Swagger and admin endpoints
	public CorsFilter corsAllowAll() {
		final UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
		final CorsConfiguration config = new CorsConfiguration();
		config.setAllowCredentials(true);
		config.addAllowedOrigin("*");
		config.addAllowedHeader("*");
		config.addAllowedMethod("*");
		source.registerCorsConfiguration("/v2/api-docs", config);
		source.registerCorsConfiguration("/admin/**", config);
		return new CorsFilter(source);
	}

	@Bean // This allows CORS to our RestControllers
	public WebMvcConfigurer corsConfigurer() {
		return new WebMvcConfigurerAdapter() {
			@Override
			public void addCorsMappings(CorsRegistry registry) {
				registry.addMapping("/**").allowedOrigins("*")
						.allowedMethods("HEAD", "GET", "DELETE", "PUT", "POST");
			}
		};
	}
}